#
# Author: Peterson Yuhala
# Docker file to test romulus in a docker container
#

# Build image: docker build -t romulus-docker .
# Create and run container: docker run --shm-size 1G -it romulus-scone
# NB: Increase the shared memory size if need be

FROM sconecuratedimages/crosscompilers:alpine3.7

RUN  apk update \
    && apk add --no-cache --virtual=.builddeps \
        bash \
        build-base \
        make \
        musl-dev \
        openjdk8 \
        wget \
        python3 \
        python3-dev \
        zip \
        libc6-compat \
        libexecinfo \
        libunwind \
        libexecinfo-dev \
        libunwind-dev \
        patch \
        perl \
        sed \
        git \
        libc-dev \
        gcc \
        g++ \
        linux-headers \
        ca-certificates \
        nss \
        && git clone https://gitlab.com/Yuhala/romulus-min.git

RUN cd /romulus-min/examples && \
    scone-g++ -O3 -g -DPWB_IS_CLFLUSH -I.. example1.cpp ../romuluslog/RomulusLog.cpp ../romuluslog/malloc.cpp ../common/ThreadRegistry.cpp -o example1

#CMD bash -c "SCONE_VERSION=1 /romulus-min/examples/example1"
